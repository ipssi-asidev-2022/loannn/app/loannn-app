import React, {useState, useEffect} from 'react';
import {
  CurrentRenderContext,
  NavigationContainer,
  TouchableOpacity,
} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {View, Image, ImageStore} from 'react-native';

// components
import Loader from '@atoms/Loader';
import {ArrowBackIcon} from '@atoms/svg/icons';

// screens
import {
  Login,
  Register,
  Home,
  HomeApp,
  FavApp,
  AddApp,
  ShopApp,
  ProfilApp,
  ScreenTools,
  ScreenSearch,
} from '@screens';

// Images
import {SearchIcons, FavIcons, AddIcons, ShopIcons} from '@atoms/svg/navIcons/';
import ProfilIcon from '@assets/Louis.jpg';
import Back from '@assets/icons/Expand_left.png';

const Root = () => {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {
          zIndex: 9999999,
          borderTopColor: '#dddddd',
          borderTopWidth: 1,
          height: 70,
          backgroundColor: '#ffffff',
        },
      }}>
      <Tab.Screen
        name="HomeApp"
        component={HomeApp}
        options={{
          tabBarIcon: ({focused}) => (
            <View>
              <SearchIcons
                resizeMode="contain"
                style={{
                  stroke: focused ? '#000000' : '#8d8d8d',
                }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="FavApp"
        component={FavApp}
        options={{
          tabBarIcon: ({focused}) => (
            <View>
              <FavIcons
                resizeMode="contain"
                style={{
                  marginTop: 5,
                  stroke: focused ? '#a80000' : '#8d8d8d',
                  fill: focused ? '#a80000' : 'transparent',
                }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="AddApp"
        component={AddApp}
        options={{
          tabBarIcon: ({focused}) => (
            <View>
              <AddIcons
                resizeMode="contain"
                style={{
                  stroke: focused ? '#000000' : '#8d8d8d',
                }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="ShopApp"
        component={ShopApp}
        options={{
          headerShown: false,
          tabBarIcon: ({focused}) => (
            <View>
              <ShopIcons
                resizeMode="contain"
                style={{
                  marginTop: 5,
                  stroke: focused ? '#000000' : '#8d8d8d',
                }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="ProfilApp"
        component={ProfilApp}
        options={{
          headerShown: false,
          tabBarIcon: ({focused}) => (
            <View>
              <Image
                source={ProfilIcon}
                resizeMode="cover"
                style={{
                  width: 40,
                  height: 40,
                  borderColor: focused ? '#F1B02D' : 'transparent',
                  borderWidth: focused ? 2 : 0,
                  borderRadius: 20 / 2,
                }}
              />
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const App = () => {
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoader(false);
    }, 2000);
  }, []);

  const Stack = createNativeStackNavigator();

  return loader ? (
    <Loader />
  ) : (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          animation: 'slide_from_right',
        }}
        initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            title: null,
            headerTransparent: true,
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            title: null,
            headerTransparent: true,
            headerShown: false,
            headerTintColor: '#ffffff',
          }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{
            title: null,
            headerTransparent: true,
            headerShown: false,
            headerTintColor: '#ffffff',
          }}
        />
        <Stack.Screen
          name="HomeApp"
          component={HomeApp}
          options={{
            headerLeft: null,
            title: null,
            headerTransparent: true,
            headerShown: false,
            headerTintColor: '#ffffff',
          }}
        />
        <Stack.Screen
          name="Root"
          component={Root}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ScreenTools"
          component={ScreenTools}
          options={{
            title: null,
            headerTransparent: true,
            headerShown: false,
            animation: 'slide_from_bottom',
          }}
        />
        <Stack.Screen
          name="ScreenSearch"
          component={ScreenSearch}
          options={{
            title: null,
            headerTransparent: true,
            headerShown: false,
            animation: 'slide_from_bottom',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

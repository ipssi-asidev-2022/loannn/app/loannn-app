FROM node:16-alpine as builder
WORKDIR /app
COPY package.json ./
RUN yarn

FROM menny/android

# install OS packages
RUN apt-get -qy update && \
    apt-get -qy install ruby ruby-dev curl gnupg \
    # We use this for xxd hex->binary
    vim-common
RUN curl -sL https://deb.nodesource.com/setup_16.x  | bash -
RUN apt-get -y install nodejs

# install FastLane
COPY Gemfile.lock Gemfile ./
RUN gem install bundler
# # might help if rubygems is outdated / broken
RUN gem update --system
# # might help if bundler is outdated / broken
RUN gem update bundler
# # update dependencies
RUN bundle update
# # install dependencies
RUN bundle install --path vendor/bundle/


COPY --from=builder /app/node_modules node_modules
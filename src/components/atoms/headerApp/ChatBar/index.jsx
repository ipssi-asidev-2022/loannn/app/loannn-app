import React from 'react';
import {View, Image} from 'react-native';
// assets
import {TchatBarIcons} from '@atoms/svg/headerIcons/';
// styles
import chatBarStyle from './chatbar.scss';

const ChatBar = ({headerShown}) => {
  return (
    <View style={chatBarStyle.container}>
      <View
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
          height: '100%',
          width: '100%',
          borderRadius: 20,
          backgroundColor: headerShown ? '#eeeeee' : '#fff',
        }}>
        <TchatBarIcons
          resizeMode="contain"
          style={{
            marginTop: 3,
            marginLeft: 3,
            stroke: '#F1B02D',
            backgroundColor: headerShown ? '#eeeeee' : '#fff',
          }}
        />
      </View>
    </View>
  );
};

export default ChatBar;

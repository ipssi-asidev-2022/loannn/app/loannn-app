import React, {useState} from 'react';
import {View, Text, ScrollView, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

// assets
import {SearchBarIcons} from '@atoms/svg/headerIcons/';

// styles
import searchBarStyle from './searchbar.scss';

const SearchBar = ({headerShown}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={searchBarStyle.container}
      onPress={() => navigation.navigate('ScreenSearch')}>
      <View
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
          height: 60,
          width: '100%',
          borderRadius: 20,
          backgroundColor: headerShown ? '#eeeeee' : '#fff',
        }}>
        <SearchBarIcons
          resizeMode="contain"
          style={{
            marginTop: 5,
            stroke: '#F1B02D',
          }}
        />
        <Text style={searchBarStyle.placeholder}>Que recherchez vous ?</Text>
      </View>
    </TouchableOpacity>
  );
};

export default SearchBar;

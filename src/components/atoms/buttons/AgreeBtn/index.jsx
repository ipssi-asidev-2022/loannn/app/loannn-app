import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
// styles
import agreeBtnStyle from './agreeBtn.scss';

/**
 * AgreeBtn
 * ? destructuring props ? ou reinseigner tout les params ?
 * @param {*}
 * @returns button
 */
const AgreeBtn = ({...props}) => {
  return (
    <View style={agreeBtnStyle.container}>
      <TouchableOpacity style={agreeBtnStyle.button} {...props}>
        <Text style={agreeBtnStyle.text}>Accepter & S'inscrire</Text>
      </TouchableOpacity>
    </View>
  );
};

export default AgreeBtn;

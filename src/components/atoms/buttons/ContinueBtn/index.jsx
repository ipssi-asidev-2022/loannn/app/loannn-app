import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
// styles
import continueBtnStyle from './continueBtn.scss';

/**
 * ContinueBtn
 * ? destructuring props ? ou reinseigner tout les params ?
 * @param {*}
 * @returns button
 */
const ContinueBtn = ({...props}) => {
  return (
    <View style={continueBtnStyle.container}>
      <TouchableOpacity style={continueBtnStyle.button} {...props}>
        <Text style={continueBtnStyle.text}>S'inscrire</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ContinueBtn;

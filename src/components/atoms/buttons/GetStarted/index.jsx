import React from 'react';
import {View, Text, TouchableOpacity, onPress, Image} from 'react-native';
// assets
// styles
import getstartedStyle from './getstarted.scss';

const GetStarted = () => {
  const C = props => <Text style={{color: '#f1b02d'}}>{props.children}</Text>;
  return (
    <View style={getstartedStyle.container}>
      <TouchableOpacity onPress={onPress} style={getstartedStyle.button}>
        <Text style={getstartedStyle.text}>
          <C>Commencer</C> maintenant !
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default GetStarted;

import React from 'react';
import {View, Text, TouchableOpacity, onPress, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';

// assets
import {SendIcon, FavIcon} from '@atoms/svg/headerIcons';

// styles
import style from './style.scss';

const LoanItBtn = () => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity style={style.button}>
      <Text style={style.buttonName}>Emprunter</Text>
      <SendIcon />
    </TouchableOpacity>
  );
};

export default LoanItBtn;

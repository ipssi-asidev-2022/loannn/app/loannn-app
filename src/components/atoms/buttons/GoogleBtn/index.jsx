import React from 'react';
import {View, Text, TouchableOpacity, onPress, Image} from 'react-native';
// asstes
import logoGG from '@assets/icons/Google.png';
// styles
import googleBtnStyle from './googleBtn.scss';

/**
 * GoogleBtn
 * @returns button
 */
const GoogleBtn = () => {
  return (
    <View style={googleBtnStyle.container}>
    <TouchableOpacity onPress={onPress} style={googleBtnStyle.button}>
      <Image source={logoGG} height="200px" width="200px" />
      <Text style={googleBtnStyle.text}>Continue with Google</Text>
    </TouchableOpacity>
  </View>
  );
};

export default GoogleBtn;

import React from 'react';
import {View, Text, TouchableOpacity, onPress, Image} from 'react-native';
// assets
import logoFB from '@assets/icons/Facebook.png';
// styles
import facebookBtnStyle from './facebookBtn.scss';

/**
 * FacebookBtn
 * @returns button
 */
const FacebookBtn = () => {
  return (
    <View style={facebookBtnStyle.container}>
      <TouchableOpacity onPress={onPress} style={facebookBtnStyle.button}>
        <Image source={logoFB} height="200px" width="200px" />
        <Text style={facebookBtnStyle.text}>Continue with Facebook</Text>
      </TouchableOpacity>
    </View>
  );
};

export default FacebookBtn;

import React from 'react';
import {View, Text, TouchableOpacity, onPress} from 'react-native';
import connectBtnStyle from './connectBtn.scss';

const ConnectBtn = ({...props}) => {
  return (
    <View style={connectBtnStyle.container}>
      <TouchableOpacity style={connectBtnStyle.button} {...props}>
        <Text style={connectBtnStyle.text}>Connexion</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ConnectBtn;

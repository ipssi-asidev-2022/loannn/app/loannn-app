import AgreeBtn from '@atoms/buttons/AgreeBtn';
import ContinueBtn from '@atoms/buttons/ContinueBtn';
import ConnectBtn from '@atoms/buttons/ConnectBtn';
import FacebookBtn from '@atoms/buttons/FacebookBtn';
import GoogleBtn from '@atoms/buttons/GoogleBtn';
import LogoutBtn from '@atoms/buttons/LogoutBtn';

export {AgreeBtn, ContinueBtn, ConnectBtn, FacebookBtn, GoogleBtn, LogoutBtn};

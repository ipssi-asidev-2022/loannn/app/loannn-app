import React from 'react';
import {View, Text, TouchableOpacity, } from 'react-native';
import {useNavigation} from '@react-navigation/native';
// services
import {logout} from '@services/strapi/user';
// styles
import logoutBtnStyle from './logoutBtn.scss';

/**
 * LogoutBtn
 * ? logique dans un atoms ?
 * ! redirige vers la screen Login
 * @returns button
 */
const LogoutBtn = () => {
  const navigation = useNavigation();

  const handlePress = async () => {
    await logout();
    navigation.navigate('Login');
  };

  return (
    <View style={logoutBtnStyle.container}>
      <TouchableOpacity
        style={logoutBtnStyle.button}
        onPress={() => handlePress('handlePress:')}>
        <Text style={logoutBtnStyle.text}>Se déconecter</Text>
      </TouchableOpacity>
    </View>
  );
};

export default LogoutBtn;

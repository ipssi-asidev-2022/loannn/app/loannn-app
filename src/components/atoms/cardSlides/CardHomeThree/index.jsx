import React from 'react';
import {View, Text, Image} from 'react-native';
// assets
import ElecTools from '@assets/tools/electrique.png';
// styles
import cardStyles from '../cardstyles.scss';

const CardHomeThree = () => {
  return (
    <View style={cardStyles.container}>
      <View style={cardStyles.colorThree}>
        <View style={cardStyles.iconCat}>
          <View style={cardStyles.circle}>
            <Image source={ElecTools} style={cardStyles.icon} />
          </View>
        </View>
        <View style={cardStyles.textCat}>
          <Text style={cardStyles.title}>Electroportatif</Text>
        </View>
      </View>
    </View>
  );
};

export default CardHomeThree;

import React from 'react';
import {View, Text, Image} from 'react-native';
// assets
import AtelierTools from '@assets/tools/toolbox.png';
// styles
import cardStyles from '../cardstyles.scss';

const CardHomeFour = () => {
  return (
    <View style={cardStyles.container}>
      <View style={cardStyles.colorFour}>
        <View style={cardStyles.iconCat}>
          <View style={cardStyles.circle}>
            <Image source={AtelierTools} style={cardStyles.icon} />
          </View>
        </View>
        <View style={cardStyles.textCat}>
          <Text style={cardStyles.title}>Matériel d'atelier</Text>
        </View>
      </View>
    </View>
  );
};

export default CardHomeFour;

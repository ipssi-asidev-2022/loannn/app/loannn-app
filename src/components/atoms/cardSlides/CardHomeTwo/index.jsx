import React from 'react';
import {View, Text, Image} from 'react-native';
// assets
import GardenTools from '@assets/tools/hache.png';
// styles
import cardStyles from '../cardstyles.scss';

const CardHomeTwo = () => {
  return (
    <View style={cardStyles.container}>
      <View style={cardStyles.colorTwo}>
        <View style={cardStyles.iconCat}>
          <View style={cardStyles.circle}>
            <Image source={GardenTools} style={cardStyles.icon} />
          </View>
        </View>
        <View style={cardStyles.textCat}>
          <Text style={cardStyles.title}>Jardin</Text>
        </View>
      </View>
    </View>
  );
};

export default CardHomeTwo;

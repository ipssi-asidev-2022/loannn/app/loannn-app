import React from 'react';
import {View, Text, Image} from 'react-native';
// assets
import KeyTools from '@assets/tools/clé.png';
// styles
import cardStyles from '../cardstyles.scss';

const CardHomeOne = () => {
  return (
    <View style={cardStyles.container}>
      <View style={cardStyles.colorOne}>
        <View style={cardStyles.iconCat}>
          <View style={cardStyles.circle}>
            <Image source={KeyTools} style={cardStyles.icon} />
          </View>
        </View>
        <View style={cardStyles.textCat}>
          <Text style={cardStyles.title}>Outillage à main</Text>
        </View>
      </View>
    </View>
  );
};

export default CardHomeOne;

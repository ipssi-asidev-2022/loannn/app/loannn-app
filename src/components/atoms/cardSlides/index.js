import CardHomeOne from '@atoms/cardSlides/CardHomeOne';
import CardHomeTwo from '@atoms/cardSlides/CardHomeTwo';
import CardHomeThree from '@atoms/cardSlides/CardHomeThree';
import CardHomeFour from '@atoms/cardSlides/CardHomeFour';

export {CardHomeOne, CardHomeTwo, CardHomeThree, CardHomeFour};

import {View, Text, ScrollView} from 'react-native';
import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

// components
import ToolsSkeleton from '@organisms/ToolsSkeleton';
// styles
import bottomTabStyles from './style.scss';

function ToolsAvailable() {
  return <ToolsSkeleton />;
}

function ToolsUnavailable() {
  return <ToolsSkeleton />;
}

const BottomTabView = () => {
  const Tab = createMaterialTopTabNavigator();
  return (
    <View style={bottomTabStyles.container}>
      <Tab.Navigator
        screenOptions={{
          headerTitle: ({focused}) => ({color: focused ? '#000' : '#8d8d8d'}),
          tabBarLabelStyle: {
            fontSize: 20,
            fontFamily: 'Satoshi-Regular',
            textTransform: 'none',
            marginTop: 6,
          },
          tabBarStyle: {
            backgroundColor: '#fff',
            height: 60,
            width: '100%',
            shadowColor: 'transparent',
            borderBottomColor: '#dddddd',
            borderBottomWidth: 1,
          },
          tabBarIndicatorStyle: {backgroundColor: '#F1B02D'},
          tabBarPressColor: 'transparent',
        }}>
        <Tab.Screen name="Disponibles" component={ToolsAvailable} />
        <Tab.Screen name="Empruntés" component={ToolsUnavailable} />
      </Tab.Navigator>
    </View>
  );
};

export default BottomTabView;

import {View, Text} from 'react-native';
import React from 'react';

// styles
import style from '../style.scss';

const CatThree = () => {
  return (
    <View style={style.cardThree}>
      <View style={style.card}>
        <Text>index</Text>
      </View>
    </View>
  );
};

export default CatThree;

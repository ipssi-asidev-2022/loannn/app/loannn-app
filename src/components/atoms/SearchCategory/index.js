import CatOne from '@atoms/SearchCategory/CatOne';
import CatTwo from '@atoms/SearchCategory/CatTwo';
import CatThree from '@atoms/SearchCategory/CatThree';
import CatFour from '@atoms/SearchCategory/CatFour';

export {CatOne, CatTwo, CatThree, CatFour};

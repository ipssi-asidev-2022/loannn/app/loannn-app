import {View, Text} from 'react-native';
import React from 'react';

// styles
import style from '../style.scss';

const CatOne = () => {
  return (
    <View style={style.cardOne}>
      <View style={style.card}>
        <Text>index</Text>
      </View>
    </View>
  );
};

export default CatOne;

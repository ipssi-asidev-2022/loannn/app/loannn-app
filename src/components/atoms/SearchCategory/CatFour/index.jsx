import {View, Text} from 'react-native';
import React from 'react';

// styles
import style from '../style.scss';

const CatFour = () => {
  return (
    <View style={style.cardFour}>
      <View style={style.card}>
        <Text>index</Text>
      </View>
    </View>
  );
};

export default CatFour;

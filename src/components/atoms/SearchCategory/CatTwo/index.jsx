import {View, Text} from 'react-native';
import React from 'react';

// styles
import style from '../style.scss';

const CatTwo = () => {
  return (
    <View style={style.cardTwo}>
      <View style={style.card}>
        <Text>index</Text>
      </View>
    </View>
  );
};

export default CatTwo;

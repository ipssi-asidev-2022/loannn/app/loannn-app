import React from 'react';
import {View, Text, Image} from 'react-native';
// styles
import bannerStyles from '../attach.scss';
// assets
import PartagerImg from '@assets/img/Partager.png';
import EcoImg from '@assets/img/Eco.png';

const EcoBanner = () => {
  return (
    <View style={bannerStyles.bannerTwo}>
      <Text style={bannerStyles.bannerTitleTwo}>
        Continuez de faire vivre vos outils ♻️
      </Text>
      <Image source={EcoImg} style={bannerStyles.bannerImg} />
    </View>
  );
};

export default EcoBanner;

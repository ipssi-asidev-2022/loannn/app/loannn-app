import React from 'react';
import {View, Text, Image} from 'react-native';
// styles
import bannerStyles from '../attach.scss';
// assets
import PartagerImg from '@assets/img/Partager.png';
import EcoImg from '@assets/img/Eco.png';

const SocialBanner = () => {
  return (
    <View style={bannerStyles.bannerOne}>
      <Text style={bannerStyles.bannerTitleOne}>
        Découvrez des personnes passionnés 🤝
      </Text>
      <Image source={PartagerImg} style={bannerStyles.bannerImg} />
    </View>
  );
};

export default SocialBanner;

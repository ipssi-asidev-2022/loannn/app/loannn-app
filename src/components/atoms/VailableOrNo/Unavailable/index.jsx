import {View, Text} from 'react-native';
import React from 'react';

// Styles
import styles from '../styles.scss';

const Unavailable = () => {
  return (
    <View style={styles.container}>
      <View style={styles.gray}>
        <Text style={styles.textTwo}>Indisponible</Text>
      </View>
    </View>
  );
};

export default Unavailable;

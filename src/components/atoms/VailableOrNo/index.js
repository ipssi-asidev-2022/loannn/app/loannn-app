import Available from '@atoms/VailableOrNo/Available';
import Unavailable from '@atoms/VailableOrNo/Unavailable';

export {Available, Unavailable};

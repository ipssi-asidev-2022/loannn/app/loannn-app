import {View, Text} from 'react-native';
import React from 'react';

// Styles
import styles from '../styles.scss';

const Available = () => {
  return (
    <View style={styles.container}>
      <View style={styles.green}>
        <Text style={styles.text}>Disponible</Text>
      </View>
    </View>
  );
};

export default Available;

import {View, TextInput} from 'react-native';
import React from 'react';

// styles
import style from './style.scss';

const InputResearch = () => {
  return (
    <View style={style.container}>
      <TextInput
        style={style.input}
        placeholder="Taper le nom de l'outil recherché"
      />
    </View>
  );
};

export default InputResearch;

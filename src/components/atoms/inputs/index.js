import InputMail from '@atoms/inputs/InputMail';
import InputName from '@atoms/inputs/InputName';
import InputPwd from '@atoms/inputs/InputPwd';

export {InputMail, InputName, InputPwd};

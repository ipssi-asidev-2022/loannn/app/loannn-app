import React from 'react';
import {View, TextInput, Text} from 'react-native';
// styles
import inputNameStyle from './inputName.scss';

/**
 * InputMail
 * ? destructuring props ? ou reinseigner tout les params ?
 * @param {*} value, onChangeText
 * @returns input
 */
const InputName = ({value, onChangeText, ...props}) => {
  return (
    <View style={inputNameStyle.container}>
      <Text style={inputNameStyle.label}>Nom d'utilisateur</Text>
      <TextInput
        style={inputNameStyle.input}
        onChangeText={onChangeText}
        value={value}
        placeholder="Nom d'utilisateur"
        {...props}
      />
    </View>
  );
};

export default InputName;

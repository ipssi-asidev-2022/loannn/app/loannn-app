import React from 'react';
import {View, TextInput, Text} from 'react-native';
// styles
import inputMailStyle from './inputMail.scss';

/**
 * InputMail
 * ? destructuring props ? ou reinseigner tout les params ?
 * @param {*} value, onChangeText
 * @returns input
 */
const InputMail = ({value, onChangeText, ...props}) => {
  return (
    <View style={inputMailStyle.container}>
    <Text style={inputMailStyle.label}>Email</Text>
    <TextInput
      style={inputMailStyle.input}
      onChangeText={onChangeText}
      value={value}
      placeholder="Email"
      {...props}
    />
  </View>
  );
};

export default InputMail;

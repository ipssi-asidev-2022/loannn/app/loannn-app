import React from 'react';
import {View, TextInput, Text} from 'react-native';
// styles
import inputPwdStyle from './inputPwd.scss';

/**
 * InputPwd
 * ? destructuring props ? ou reinseigner tout les params ?
 * @param {*} value, onChangeText
 * @returns button
 */
const InputPwd = ({value, onChangeText, ...props}) => {
  return (
    <View style={inputPwdStyle.container}>
    <Text style={inputPwdStyle.label}>Mot de passe</Text>
    <TextInput
      style={inputPwdStyle.input}
      onChangeText={onChangeText}
      value={value}
      placeholder="Mot de passe"
      {...props}
    />
  </View>
  );
};

export default InputPwd;

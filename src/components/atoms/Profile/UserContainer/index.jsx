import React from 'react';
import {View, onPress, Image, Text} from 'react-native';
// Assets
import ProfilImg from '@assets/Louis.jpg';
// Styles
import userContainerStyle from './userContainer.scss';

export const UserContainer = () => {
  return (
    <View style={userContainerStyle.userContainer}>
      <View style={userContainerStyle.topBlock}>
        <View style={userContainerStyle.leftBlock}>
          <Image source={ProfilImg} style={userContainerStyle.profilImg} />
        </View>
        <View style={userContainerStyle.rightBlock}>
          <View style={userContainerStyle.tools}>
            <Text style={userContainerStyle.label}>Disponibles</Text>
            <Text style={userContainerStyle.number}>14</Text>
          </View>
          <View style={userContainerStyle.untools}>
            <Text style={userContainerStyle.label}>Empruntés</Text>
            <Text style={userContainerStyle.number}>3</Text>
          </View>
          <View style={userContainerStyle.avis}>
            <Text style={userContainerStyle.label}>Évaluations</Text>
            <Text style={userContainerStyle.number}>26</Text>
          </View>
        </View>
      </View>
      <View style={userContainerStyle.bottomBlock}>
        <Text style={userContainerStyle.infos}>Louis</Text>
        <View style={userContainerStyle.stars}></View>
      </View>
    </View>
  );
};

export default UserContainer;

import React from 'react';
import styled from 'styled-components/native';
// assets
import LoaderImg from '@atoms/svg/logoIcons/logo200.jsx';

/**
 * Loader
 * ? on utilise styled-components ? ou full scss ?
 * @returns loader
 */
const Loader = () => {
  return (
    <Container>
      <DivParent>
        <LoaderImg />
      </DivParent>
      <TextLoan>Loannn</TextLoan>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: #050505;
`;

const DivParent = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

const TextLoan = styled.Text`
  color: #fff;
  font-family: grifterbold;
  font-size: 24px;
  margin-bottom: 20px;
`;

export default Loader;

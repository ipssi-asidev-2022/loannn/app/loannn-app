import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';

// Assets
import Template from '@assets/Louis.jpg';
// Styles
import favoris from './favoris.scss';
// Components
import {Available, Unavailable} from '@atoms/VailableOrNo';

const Skeleton = () => {
  return (
    <View style={favoris.container}>
      <View style={favoris.skeleton}>
        <View style={favoris.blockImg}>
          <Image source={Template} style={favoris.img} />
        </View>
        <View style={favoris.blockTitle}>
          <Text style={favoris.title}>Nom de l'outil</Text>
          <Text style={favoris.note}>Note du proprio</Text>
        </View>
        <View style={favoris.blockDispo}>
          <Unavailable />
        </View>
      </View>
      <View style={favoris.skeleton}>
        <View style={favoris.blockImg}>
          <Image source={Template} style={favoris.img} />
        </View>
        <View style={favoris.blockTitle}>
          <Text style={favoris.title}>Nom de l'outil</Text>
          <Text style={favoris.note}>Note du proprio</Text>
        </View>
        <View style={favoris.blockDispo}>
          <Available />
        </View>
      </View>
    </View>
  );
};

export default Skeleton;

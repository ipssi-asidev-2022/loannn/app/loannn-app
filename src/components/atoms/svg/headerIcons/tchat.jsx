import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const SvgComponent = props => (
  <Svg
    width={30}
    height={30}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path
      d="M26 9c0-3.771 0-5.657-1.172-6.828C23.657 1 21.771 1 18 1H9C5.229 1 3.343 1 2.172 2.172 1 3.343 1 5.229 1 9v15c0 .943 0 1.414.293 1.707C1.586 26 2.057 26 3 26h15c3.771 0 5.657 0 6.828-1.172C26 23.657 26 21.771 26 18V9Z"
      stroke="#F1B02D"
      strokeWidth={2}
    />
    <Path
      d="M7 9h10M7 16h6"
      stroke="#F1B02D"
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </Svg>
);

export default SvgComponent;

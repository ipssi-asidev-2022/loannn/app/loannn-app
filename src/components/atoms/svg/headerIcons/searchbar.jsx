import * as React from 'react';
import Svg, {Path, Ellipse} from 'react-native-svg';

const SvgComponent = props => (
  <Svg
    width={24}
    height={24}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path
      d="M16.517 16.609 21 21"
      stroke="#F1B02D"
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Ellipse
      cx={9.952}
      cy={10.005}
      rx={8.952}
      ry={9.005}
      stroke="#F1B02D"
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </Svg>
);

export default SvgComponent;

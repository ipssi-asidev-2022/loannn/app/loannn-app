import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const SvgComponent = props => (
  <Svg
    style={{marginTop: 5}}
    width={21}
    height={21}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path
      d="m2.887 6.565.534 1.057c.34.673.509 1.01.509 1.378 0 .369-.17.705-.51 1.378l-.533 1.057c-1.518 3.01-2.278 4.514-1.686 5.26.592.747 1.995.052 4.8-1.337l7.696-3.813C15.899 10.454 17 9.908 17 9c0-.908-1.101-1.454-3.303-2.545L6 2.642C3.196 1.252 1.793.558 1.201 1.304c-.592.747.168 2.251 1.686 5.261Z"
      stroke="#000"
      strokeWidth={2}
    />
  </Svg>
);

export default SvgComponent;

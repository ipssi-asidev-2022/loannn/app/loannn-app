import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const SvgComponent = props => (
  <Svg
    width={24}
    height={24}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path
      d="m3.533 12.734 8.248 8.523c.338.35.507.524.719.524.212 0 .38-.174.719-.524l8.248-8.523a6.134 6.134 0 0 0 .605-7.802l-.638-.904c-2.172-3.08-6.88-2.597-8.38.862a.604.604 0 0 1-1.108 0c-1.5-3.459-6.207-3.943-8.38-.862l-.638.904a6.134 6.134 0 0 0 .605 7.802Z"
      stroke="#A80000"
      strokeWidth={2}
    />
  </Svg>
);

export default SvgComponent;

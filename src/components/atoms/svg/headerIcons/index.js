import SearchBarIcons from '@atoms/svg/headerIcons/searchbar.jsx';
import TchatBarIcons from '@atoms/svg/headerIcons/tchat.jsx';
import SettingsBarIcons from '@atoms/svg/headerIcons/settings.jsx';
import SendIcon from '@atoms/svg/headerIcons/send.jsx';
import FavIcon from '@atoms/svg/headerIcons/favoris.jsx';

export {SearchBarIcons, TchatBarIcons, SettingsBarIcons, SendIcon, FavIcon};

import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const SvgComponent = props => (
  <Svg width={35} height={35} xmlns="http://www.w3.org/2000/svg" {...props}>
    <Path
      d="M17 2v30M32 17H2"
      strokeWidth={3}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </Svg>
);

export default SvgComponent;

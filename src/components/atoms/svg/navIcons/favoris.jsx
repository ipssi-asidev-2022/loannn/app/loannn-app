import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const SvgComponent = props => (
  <Svg width={40} height={40} xmlns="http://www.w3.org/2000/svg" {...props}>
    <Path
      d="m3.946 17.027 11.835 12.23c.338.35.507.524.719.524.212 0 .38-.174.719-.524l11.835-12.23a8.588 8.588 0 0 0 .847-10.922l-.893-1.266C25.966.526 19.376 1.204 17.276 6.046a.845.845 0 0 1-1.552 0c-2.1-4.842-8.69-5.52-11.732-1.207l-.893 1.266a8.588 8.588 0 0 0 .847 10.922Z"
      strokeWidth={3}
    />
  </Svg>
);

export default SvgComponent;

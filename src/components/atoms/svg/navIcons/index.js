import SearchIcons from '@atoms/svg/navIcons/search.jsx';
import FavIcons from '@atoms/svg/navIcons/favoris.jsx';
import AddIcons from '@atoms/svg/navIcons/add.jsx';
import ShopIcons from '@atoms/svg/navIcons/shop.jsx';

export {SearchIcons, FavIcons, AddIcons, ShopIcons};

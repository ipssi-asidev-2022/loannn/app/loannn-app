import * as React from 'react';
import Svg, {Path, Ellipse} from 'react-native-svg';

const SvgComponent = props => (
  <Svg width={35} height={35} xmlns="http://www.w3.org/2000/svg" {...props}>
    <Path
      d="M23.723 23.852 30 30"
      strokeWidth={3}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Ellipse
      cx={14.533}
      cy={14.607}
      rx={12.533}
      ry={12.607}
      strokeWidth={3}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </Svg>
);

export default SvgComponent;

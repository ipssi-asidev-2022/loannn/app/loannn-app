import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const SvgComponent = props => (
  <Svg
    width={24}
    height={24}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path
      d="m13 11 .64-.768.922.768-.922.768L13 11ZM1.64.232l12 10-1.28 1.536-12-10L1.64.232Zm12 11.536-12 10-1.28-1.536 12-10 1.28 1.536Z"
      fill="#000"
    />
  </Svg>
);

export default SvgComponent;

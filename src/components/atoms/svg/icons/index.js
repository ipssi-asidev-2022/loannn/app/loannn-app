import ArrowIcon from '@atoms/svg/icons/arrow.jsx';
import ArrowBackIcon from '@atoms/svg/icons/arrowBack.jsx';

export {ArrowIcon, ArrowBackIcon};

import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const LogoLoannn50 = props => (
  <Svg
    width={50}
    height={51}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path d="m14.412 50.875 33.944-20.023-30.73.123" fill="#E69908" />
    <Path
      d="M33.944 11.042 0 31.065l30.73-.123M16.053 40.732 14.41 50.875l33.945-20.02"
      fill="#E69908"
    />
    <Path d="m15.212 45.953 33.944-20.022-30.73.122" fill="#F1B02D" />
    <Path d="M34.735 6.123.791 26.145l30.73-.122" fill="#F1B02D" />
    <Path d="M19.27 20.832 50 20.709 16.055 40.733" fill="#FAC762" />
    <Path d="M35.586.88 1.642 20.903l30.73-.123" fill="#FAC762" />
    <Path
      d="M50 20.709c-11.303 6.666-22.606 13.332-33.908 20l-.044.025-1.637 10.146 33.95-20.027M1.642 20.902 0 31.065l17.626-.09 1.643-10.143-17.627.07Z"
      fill="#FCB100"
      opacity={0.3}
    />
  </Svg>
);

export default LogoLoannn50;

import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const LogoLoannn = props => (
  <Svg
    width={200}
    height={201}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path d="m57.65 200.862 135.776-80.091-122.922.49" fill="#E69908" />
    <Path
      d="M135.776 41.529 0 121.62l122.922-.49M64.21 160.289l-6.566 40.573 135.781-80.078"
      fill="#E69908"
    />
    <Path d="m60.847 181.174 135.776-80.091-122.922.491" fill="#F1B02D" />
    <Path d="M138.941 21.852 3.165 101.943l122.922-.49" fill="#F1B02D" />
    <Path d="m77.077 80.688 122.921-.49L64.223 160.29" fill="#FAC762" />
    <Path d="M142.342.88 6.567 80.97l122.921-.49" fill="#FAC762" />
    <Path
      d="M200 80.196a2084862.01 2084862.01 0 0 0-135.807 80.1l-6.55 40.584 135.805-80.109M6.566 80.97 0 121.62l70.504-.359 6.572-40.573-70.51.282Z"
      fill="#FCB100"
      opacity={0.3}
    />
  </Svg>
);

export default LogoLoannn;

import React from 'react';
import {Text, View, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';

// assets
import EcoBanner from '@assets/img/Eco.png';

// styles
import toolsSkeletonStyles from './toolsSkeleton.scss';

const ToolsSkeleton = () => {
  const navigation = useNavigation();

  return (
    <View style={toolsSkeletonStyles.container}>
      <View style={toolsSkeletonStyles.tools}>
        <Image source={EcoBanner} style={toolsSkeletonStyles.image} />
      </View>
      <View style={toolsSkeletonStyles.tools}>
        <Image source={EcoBanner} style={toolsSkeletonStyles.image} />
      </View>
      <View style={toolsSkeletonStyles.tools}>
        <Image source={EcoBanner} style={toolsSkeletonStyles.image} />
      </View>
      <View style={toolsSkeletonStyles.tools}>
        <Image source={EcoBanner} style={toolsSkeletonStyles.image} />
      </View>
      <View style={toolsSkeletonStyles.tools}>
        <Image source={EcoBanner} style={toolsSkeletonStyles.image} />
      </View>
      <Text onPress={() => navigation.navigate('ScreenTools')}>GOOOOOOOO</Text>
    </View>
  );
};

export default ToolsSkeleton;

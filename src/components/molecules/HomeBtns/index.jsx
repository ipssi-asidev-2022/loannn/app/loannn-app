import React from 'react';
import { View, onPress} from 'react-native';
import {useNavigation} from '@react-navigation/native';
// components
// styles
import homeBtnsStyles from './homeBtns.scss';
// services
import {ContinueBtn, FacebookBtn, GoogleBtn} from '@atoms/buttons';

const HomeBtns = () => {
  const navigation = useNavigation();

  return (
    <View style={homeBtnsStyles.container}>
      <FacebookBtn onPress={onPress} />
      <GoogleBtn onPress={onPress} />
      <ContinueBtn onPress={() => navigation.navigate('Register')} />
    </View>
  );
};

export default HomeBtns;

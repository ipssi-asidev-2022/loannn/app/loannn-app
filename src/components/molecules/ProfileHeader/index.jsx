import React from 'react';
import {View, onPress, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
// components
import LogoImg from '@atoms/svg/logoIcons/logo50.jsx';
import {TchatBarIcons, SettingsBarIcons} from '@atoms/svg/headerIcons/';
// styles
import profileHeaderStyles from './profileHeader.scss';
// services

const ProfileHeader = () => {
  //   const navigation = useNavigation();

  return (
    <View style={profileHeaderStyles.header}>
      <View style={profileHeaderStyles.headLeftBlock}>
        <LogoImg />
      </View>
      <View style={profileHeaderStyles.headRightBlock}>
        <TchatBarIcons />
        <SettingsBarIcons />
      </View>
    </View>
  );
};

export default ProfileHeader;

import React from 'react';
import {Text, View} from 'react-native';
import {Formik} from 'formik';
import * as yup from 'yup';
import {useNavigation} from '@react-navigation/native';
// components
import {InputMail,InputName,InputPwd} from '@atoms/inputs';
import {AgreeBtn} from '@atoms/buttons';
// styles
import formRegisterStyle from './formRegister.scss';
// services
import {register} from '@services/strapi/user';

/**
 * FormRegister
 * ? components molecules ? ou organisms ?
 * @returns Form
 */
const FormRegister = () => {

  // ? faire un component ? si utilisé dans plusieurs components => duplication de code
  const B = props => <Text style={{fontWeight: 'bold'}}>{props.children}</Text>;
  const C = props => <Text style={{color: '#f1b02d'}}>{props.children}</Text>;

  const registerValidationSchema = yup.object().shape({
    email: yup
      .string()
      .email('Entrer une adresse email valide')
      .required('Une adresse email est requise'),
    username: yup.string().required("Un nom d'utilisateur est requis"),
    password: yup
      .string()
      .min(6, ({min}) => `Le mot de passe doit faire minimum ${min} charactères`)
      .required('Un mot de passe est requis'),
  });

  const sendData = async body => {
    const parsedUser = await registerValidationSchema.validate(body);
    const test = await register(body);
  };

  const navigation = useNavigation();

  return (
    <Formik
      validationSchema={registerValidationSchema}
      initialValues={{username: '', email: '', password: ''}}
      onSubmit={body => sendData(body)}>
      {({handleChange, handleBlur, handleSubmit, values, errors, touched, isValid}) => (
        <View style={formRegisterStyle.container}>
        {touched.email && errors.email && (
          <Text style={{fontSize: 12, color: '#FF0000'}}>
            {errors.email}
          </Text>
        )}
        <InputMail
          name="email"
          value={values.email}
          onChangeText={handleChange('email')}
          onBlur={handleBlur('email')}
          keyboardType="email-address"
        />
        {touched.username && errors.username && (
          <Text style={{fontSize: 12, color: '#FF0000'}}>
            {errors.username}
          </Text>
        )}
        <InputName
          name="username"
          value={values.username}
          onChangeText={handleChange('username')}
          onBlur={handleBlur('username')}
        />
        {touched.password && errors.password && (
          <Text style={{fontSize: 12, color: '#FF0000'}}>
            {errors.password}
          </Text>
        )}
        <InputPwd
          name="password"
          value={values.password}
          onChangeText={handleChange('password')}
          onBlur={handleBlur('password')}
          secureTextEntry
        />
        <Text style={formRegisterStyle.textTwo}>
          En validant Accepter & S'inscrire en dessous,
        </Text>
        <Text style={formRegisterStyle.textThree}>
          J'accepte{' '}
          <Text style={formRegisterStyle.textFour}>
            Les termes d'utilisations
          </Text>
        </Text>
        <AgreeBtn onPress={handleSubmit} disabled={!isValid} />
      </View>
      )}
    </Formik>
  );
};

export default FormRegister;
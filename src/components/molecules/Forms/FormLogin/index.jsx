import React from 'react';
import {Text, View} from 'react-native';
import {Formik} from 'formik';
import * as yup from 'yup';
import {useNavigation} from '@react-navigation/native';
// styles
import formLoginStyle from './formLogin.scss';
// components
import {InputMail, InputPwd} from '@atoms/inputs';
import {ConnectBtn} from '@atoms/buttons';
// services
import {login} from '@services/strapi/user';

/**
 * FormLogin
 * ? components molecules ? ou organisms ?
 * @returns Form
 */
const FormLogin = () => {
  // ? faire un component ? si utilisé dans plusieurs components => duplication de code
  const B = props => <Text style={{fontWeight: 'bold'}}>{props.children}</Text>;

  const loginValidationSchema = yup.object().shape({
    identifier: yup
      .string()
      .email('Entrer une adresse email valide')
      .required('Une adresse email est requise'),
    password: yup
      .string()
      .min(6, ({min}) => `Le password doit faire au minimum ${min} charactères`)
      .required('Un mot de passe est requis'),
  });

  const navigation = useNavigation();

  const sendData = async body => {
    const parsedUser = await loginValidationSchema.validate(body);
    const test = await login(parsedUser);
    if (test.hasOwnProperty('jwt')) navigation.navigate('Register');
  };

  return (
    <Formik
      validationSchema={loginValidationSchema}
      initialValues={{identifier: '', password: ''}}
      onSubmit={body => sendData(body)}>
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        touched,
        isValid,
      }) => (
        <View style={formLoginStyle.container}>
          {touched.identifier && errors.identifier && (
            <Text style={{fontSize: 12, color: '#FF0000'}}>
              {errors.identifier}
            </Text>
          )}
          <InputMail
            name="identifier"
            value={values.identifier}
            onChangeText={handleChange('identifier')}
            onBlur={handleBlur('identifier')}
          />
          {touched.password && errors.password && (
            <Text style={{fontSize: 12, color: '#FF0000'}}>
              {errors.password}
            </Text>
          )}
          <InputPwd
            name="password"
            value={values.password}
            onChangeText={handleChange('password')}
            onBlur={handleBlur('password')}
            secureTextEntry
          />

          <ConnectBtn onPress={handleSubmit} disabled={!isValid} />
        </View>
      )}
    </Formik>
  );
};

export default FormLogin;

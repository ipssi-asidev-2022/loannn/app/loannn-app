import FormLogin from '@molecules/Forms/FormLogin';
import FormRegister from '@molecules/Forms/FormRegister';

export {FormLogin, FormRegister};

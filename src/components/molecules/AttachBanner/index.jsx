import React from 'react';
import {View, Text, Image} from 'react-native';
// components
import {EcoBanner, SocialBanner} from '@atoms/attach';
// styles
import attachBannerStyles from './attachbanner.scss';

const AttachBanner = () => {
  return (
    <View style={attachBannerStyles.container}>
      <Text style={attachBannerStyles.title}>
        De nombreux avantages en utilisant Loannn
      </Text>
      <EcoBanner />
      <SocialBanner />
    </View>
  );
};

export default AttachBanner;

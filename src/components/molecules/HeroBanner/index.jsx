import React from 'react';
import {View, Text, TouchableOpacity, onPress, Image} from 'react-native';
// assets
import shareBanner from '@assets/img/Share.png';
// components
import GetStarted from '@atoms/buttons/GetStarted';
// styles
import herobannerStyle from './herobanner.scss';

const HeroBanner = () => {
  const C = props => <Text style={{color: '#f1b02d'}}>{props.children}</Text>;
  return (
    <View style={herobannerStyle.homeBanner}>
      <View style={herobannerStyle.bgBlack}></View>
      <Image source={shareBanner} style={herobannerStyle.shareBanner} />
      <View style={herobannerStyle.textContainer}>
        <Text style={herobannerStyle.titleBanner}>
          Prêtez vos outils et devenez un <C>Loannner</C>.
        </Text>
      </View>
      <GetStarted style={herobannerStyle.buttonBanner} />
    </View>
  );
};

export default HeroBanner;

import React from 'react';
import {View, Text, ScrollView} from 'react-native';
// assets
// components
import {
  CardHomeOne,
  CardHomeTwo,
  CardHomeThree,
  CardHomeFour,
} from '@atoms/cardSlides';
// styles
import catCarouselStyle from './catcarousel.scss';

const CatCarousel = () => {
  return (
    <View style={catCarouselStyle.catContainer}>
      <View style={catCarouselStyle.categories}>
        <Text style={catCarouselStyle.title}>
          Parmi nos catégories vous pouvez retrouver
        </Text>
        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
          <View style={catCarouselStyle.carousel}>
            <View style={catCarouselStyle.voide}></View>
            <CardHomeOne />
            <CardHomeTwo />
            <CardHomeThree />
            <CardHomeFour />
            <View style={catCarouselStyle.voide}></View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default CatCarousel;

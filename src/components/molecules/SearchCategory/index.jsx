import {View, Text} from 'react-native';
import React from 'react';

// components
import {
  CatOne,
  CatTwo,
  CatThree,
  CatFour,
} from '@atoms/SearchCategory/index.js';

//styles
import style from './style.scss';

const SearchCategory = () => {
  return (
    <View style={style.container}>
      <CatOne />
      <CatTwo />
      <CatThree />
      <CatFour />
    </View>
  );
};

export default SearchCategory;

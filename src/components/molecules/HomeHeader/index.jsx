import React, {useContext} from 'react';
import {View, TextInput, Image} from 'react-native';
// components
import ChatBar from '@atoms/headerApp/ChatBar';
import SearchBar from '@atoms/headerApp/SearchBar';
// styles
import headerStyle from './header.scss';

const Header = ({headerShown}) => {
  return (
    <View style={headerStyle.container}>
      <SearchBar headerShown={headerShown} />
      <ChatBar headerShown={headerShown} />
    </View>
  );
};
export default Header;

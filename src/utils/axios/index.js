import axios from 'axios';

import {BASE_URL_API} from '@env';

const baseURLApi = BASE_URL_API || 'http://localhost:1337/api';

/**
 * crée une instance axios
 * avec des parametres custom
 */
const api = axios.create({
  baseURL: baseURLApi,
  timeout: 2500,
  // headers: {'X-Custom-Header': 'foobar'},
});

/**
 * configureApiBearerTokenHeaders
 * ajout un token pour les requetes si nécessaire
 * @param {*} token
 */
const configureApiBearerTokenHeaders = token => {
  api.defaults.headers['Authorization'] = `Bearer ${token}`;
};
// configureAxiosHeaders(baseToken);

api.defaults.headers.post['Content-Type'] = 'application/json';

/**
 * handleError
 * retourne une erreur en fonction
 * de la réponse de la requete
 * @param {*} error
 * @returns error
 */
const handleError = error => {
  // Error 😨
  if (error.response) {
    /*
     * The request was made and the server responded with a
     * status code that falls out of the range of 2xx
     */
    return {
      status: error.response.status,
      message: error.response.data.error.message,
    };
  } else if (error.request) {
    /*
     * The request was made but no response was received, `error.request`
     * is an instance of XMLHttpRequest in the browser and an instance
     * of http.ClientRequest in Node.js
     */
    console.warn('error.request:', error.request);
  } else {
    // Something happened in setting up the request and triggered an Error
    console.warn('error.message:', error.message);
  }
  console.warn('error:', error);
};

export {api, baseURLApi, configureApiBearerTokenHeaders, handleError};

import React from 'react';
import {Text, View, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
// Components
import HomeBtns from '@molecules/HomeBtns';
// Images
import homeBg from '@assets/loannn/home.png';
import logoStart from '@assets/loannn/logoStart.png';
// Styles
import homeStyle from './home.scss';

const Home = () => {
  const C = props => <Text style={{color: '#f1b02d'}}>{props.children}</Text>;
  const navigation = useNavigation();

  return (
    <View style={{flex: 1}}>
      <View style={homeStyle.container}>
        <Image style={homeStyle.logo} source={logoStart} />
        <Image style={homeStyle.image} source={homeBg} />
        <View style={homeStyle.btnContainer}>
          <View style={homeStyle.titleContainer}>
            <Text style={homeStyle.titleOne}>Don&apos;t be alone.</Text>
            <Text
              style={homeStyle.titleTwo}
              onPress={() => navigation.navigate('Root')}>
              Be a <C>Loannner</C>.
            </Text>
          </View>
          <View style={homeStyle.line} />
          <HomeBtns />
          <View style={homeStyle.loginContainer}>
            <Text style={homeStyle.loginOne}>Vous êtes déjà membre ?</Text>
            <Text
              style={homeStyle.loginTwo}
              onPress={() => navigation.navigate('Login')}>
              <C>Connectez-vous !</C>.
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Home;

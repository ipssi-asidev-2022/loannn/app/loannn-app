import {View, Text, ScrollView} from 'react-native';
import React from 'react';

// components
import {ArrowBackIcon} from '@atoms/svg/icons';
import Categories from '@molecules/SearchCategory';
import InputResearch from '@atoms/inputs/InputResearch';

// styles
import style from './style.scss';

const ScreenSearch = () => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <ArrowBackIcon />
      <View style={style.head}>
        <InputResearch />
      </View>
      <ScrollView style={style.scrollview}>
        <Text style={style.title}>Nos Catégories</Text>
        <View style={style.parent}>
          <Categories />
        </View>
      </ScrollView>
    </View>
  );
};

export default ScreenSearch;

import React from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

// components
import {Available, Unavailable} from '@atoms/VailableOrNo';
import {SendIcon, FavIcon} from '@atoms/svg/headerIcons';
import {ArrowIcon} from '@atoms/svg/icons';
import LoanItBtn from '@atoms/buttons/LoanItBtn';
import {ArrowBackIcon} from '@atoms/svg/icons';

// assets
import EcoBanner from '@assets/img/Eco.png';
import ProfilImg from '@assets/Louis.jpg';

// styles
import screenToolsStyles from './screentools.scss';

const ScreenTools = () => {
  const navigation = useNavigation();

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <ArrowBackIcon />
      <ScrollView style={screenToolsStyles.scrollView}>
        <View style={screenToolsStyles.container}>
          <View style={screenToolsStyles.blockImage}>
            <TouchableOpacity style={screenToolsStyles.fav}>
              <FavIcon />
            </TouchableOpacity>
            <Image source={EcoBanner} style={screenToolsStyles.image} />
          </View>
          <View style={screenToolsStyles.blockInfos}>
            <View style={screenToolsStyles.topInfos}>
              <Text style={screenToolsStyles.title}>Nom de l'outil</Text>
              <Available />
              {/* <Unavailable /> */}
            </View>
            <View style={screenToolsStyles.bottomInfos}>
              <Text style={screenToolsStyles.city}>
                Nom de la ville de l'outil
              </Text>
              <Text style={screenToolsStyles.category}>
                Catégories de l'outil
              </Text>
              <Text style={screenToolsStyles.createdAt}>
                21/01/2022 à 12:00
              </Text>
            </View>
            <View style={screenToolsStyles.hr}></View>
            <LoanItBtn />
          </View>
          <View style={screenToolsStyles.hr}></View>
          <View style={screenToolsStyles.blockProfile}>
            <View style={screenToolsStyles.profile}>
              <Image source={ProfilImg} style={screenToolsStyles.profilImg} />
              <View style={screenToolsStyles.profileInfos}>
                <Text style={screenToolsStyles.name}>Louis Poulin</Text>
                <Text style={screenToolsStyles.annonces}>4 Annonces</Text>
                <Text style={screenToolsStyles.avis}>Note *****</Text>
              </View>
            </View>
            <TouchableOpacity
              style={screenToolsStyles.link}
              onPress={() => navigation.navigate('ProfilApp')}>
              <ArrowIcon />
            </TouchableOpacity>
          </View>
          <View style={screenToolsStyles.hr}></View>
          <View style={screenToolsStyles.blockDescription}>
            <Text style={screenToolsStyles.label}>Description</Text>
            <Text style={screenToolsStyles.description}>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi
              vero suscipit libero velit dolorum consequuntur maxime fugiat
              placeat, similique minima.
            </Text>
          </View>
          <View style={screenToolsStyles.hr}></View>
          <View style={screenToolsStyles.blockFaq}>
            <Text style={screenToolsStyles.label}>Comment ça marche ?</Text>
            <Text style={screenToolsStyles.response}>
              Pour pouvoir emprunter un outil cliquez sur le bouton Empruntez un
              peu plus haut, sélectionnez des dates d'emprunts et attendez la
              réponse du préteur.
            </Text>
          </View>
          <View style={screenToolsStyles.hr}></View>
        </View>
      </ScrollView>
    </View>
  );
};

export default ScreenTools;

import React, {useState} from 'react';
import {Text, View, Image, ScrollView} from 'react-native';
// Components
import HeaderProfile from '@molecules/ProfileHeader';
import UserContainer from '@atoms/Profile/UserContainer';
import BottomTabView from '@atoms/BottomTabView';
// Styles
import profilAppStyle from './profilapp.scss';

const ProfilApp = () => {
  //   const C = props => <Text style={{color: '#f1b02d'}}>{props.children}</Text>;
  //   const navigation = useNavigation();

  return (
    <View style={{flex: 1}}>
      <View style={profilAppStyle.container}>
        <HeaderProfile />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={profilAppStyle.scrollview}>
          <UserContainer />
          <BottomTabView />
        </ScrollView>
      </View>
    </View>
  );
};

export default ProfilApp;

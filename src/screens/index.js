import Login from '@screens/Login';
import Register from '@screens/Register';
import Home from '@screens/Home';
import HomeApp from '@screens/HomeApp/';
import FavApp from '@screens/FavApp/';
import AddApp from '@screens/AddApp/';
import ShopApp from '@screens/ShopApp/';
import ProfilApp from '@screens/ProfilApp/';
import ScreenTools from '@screens/ScreenTools/';
import ScreenSearch from '@screens/ScreenSearch/';

// SearchApp, AddApp, ShopApp, ProfilApp

export {
  Login,
  Register,
  Home,
  HomeApp,
  FavApp,
  AddApp,
  ShopApp,
  ProfilApp,
  ScreenTools,
  ScreenSearch,
};

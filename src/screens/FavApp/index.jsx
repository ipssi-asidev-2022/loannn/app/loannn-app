import React from 'react';
import {Text, View, Image, ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';
// Assets
import {FavIcons} from '@atoms/svg/navIcons/';
// Components
import {TchatBarIcons} from '@atoms/svg/headerIcons/';
import FavSkeleton from '@atoms/Favoris';
// Styles
import favAppStyle from './favapp.scss';

const FavApp = () => {
  //   const C = props => <Text style={{color: '#f1b02d'}}>{props.children}</Text>;
  //   const navigation = useNavigation();

  return (
    <View style={{flex: 1}}>
      <View style={favAppStyle.head}>
        <View style={favAppStyle.title}>
          <Text style={favAppStyle.text}>Mes favoris </Text>
          <FavIcons
            resizeMode="contain"
            style={{
              height: 25,
              width: 25,
              stroke: '#a80000',
              fill: '#a80000',
            }}
          />
        </View>
        <View style={favAppStyle.chatbar}>
          <TchatBarIcons />
        </View>
      </View>
      <ScrollView style={favAppStyle.container}>
        <FavSkeleton />
      </ScrollView>
    </View>
  );
};

export default FavApp;

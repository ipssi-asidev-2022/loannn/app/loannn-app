import React from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
// components
import {FormRegister} from '@molecules/Forms';
import {ArrowBackIcon} from '@atoms/svg/icons';
// assets
import logoStart from '@assets/loannn/logoStart.png';
import registerBg from '@assets/loannn/register.png';
// styles
import registerStyle from './register.scss';

/**
 * Register
 * @returns screen
 */
const Register = () => {
  const C = props => <Text style={{color: '#f1b02d'}}>{props.children}</Text>;

  return (
    <View style={{flex: 1}}>
      <ArrowBackIcon />
      <View style={registerStyle.container}>
        <ScrollView
          style={registerStyle.view}
          contentContainerStyle={{flexGrow: 1}}>
          <View style={registerStyle.loginContainer}>
            <Image style={registerStyle.image} source={registerBg} />
            <Image style={registerStyle.logo} source={logoStart} />
            <View style={registerStyle.formContainer}>
              <View style={registerStyle.titleContainer}>
                <Text style={registerStyle.titleOne}>
                  Rejoignez les <C>Loannners</C>.
                </Text>
              </View>
              <View style={registerStyle.line}></View>
              <FormRegister />
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default Register;

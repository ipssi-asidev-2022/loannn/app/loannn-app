import React, {useState, useEffect} from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
// assets
// Components
import Header from '@molecules/HomeHeader';
import HeroBanner from '@molecules/HeroBanner';
import CatCarousel from '@molecules/CatCarousel';
import AttachBanner from '@molecules/AttachBanner';
// Styles
import homeAppStyle from './homeapp.scss';

const HomeApp = ({navigation}) => {
  const C = props => <Text style={{color: '#f1b02d'}}>{props.children}</Text>;
  //   const navigation = useNavigation();

  const [headerShown, setHeaderShown] = useState(false);

  return (
    <View style={{flex: 1}}>
      <View style={homeAppStyle.container}>
        <View
          style={{
            zIndex: 99999,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            height: 90,
            backgroundColor: headerShown ? '#fff' : 'transparent',
            borderBottomColor: headerShown ? '#dddddd' : 'transparent',
            borderBottomWidth: headerShown ? 1 : 0,
          }}></View>
        <Header headerShown={headerShown} />
        <ScrollView
          style={homeAppStyle.scrollView}
          showsVerticalScrollIndicator={false}
          onScroll={event => {
            const scrolling = event.nativeEvent.contentOffset.y;

            if (scrolling > 30) {
              setHeaderShown(true);
            } else {
              setHeaderShown(false);
            }
          }}
          scrollEventThrottle={16}>
          <HeroBanner />
          <CatCarousel />
          <AttachBanner />
        </ScrollView>
      </View>
    </View>
  );
};

export default HomeApp;

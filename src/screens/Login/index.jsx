import React from 'react';
import {ScrollView, View, Image, Text, onPress, Animated} from 'react-native';
// components
import {FormLogin} from '@molecules/Forms';
import {ArrowBackIcon} from '@atoms/svg/icons';
// assets
import logoStart from '@assets/loannn/logoStart.png';
import loginBg from '@assets/loannn/login.png';
// styles
import loginStyle from './login.scss';

/**
 * Login
 * @returns screen
 */
const Login = () => {
  const C = props => <Text style={{color: '#f1b02d'}}>{props.children}</Text>;
  return (
    <View style={{flex: 1}}>
      <ArrowBackIcon />
      <View style={loginStyle.container}>
        <ScrollView
          style={loginStyle.view}
          contentContainerStyle={{flexGrow: 1}}>
          <View style={loginStyle.loginContainer}>
            <Image style={loginStyle.image} source={loginBg} />
            <Image style={loginStyle.logo} source={logoStart} />
            <View style={loginStyle.formContainer}>
              <View style={loginStyle.titleContainer}>
                <Text style={loginStyle.titleOne}>
                  Heureux de vous revoir parmis nous très cher <C>Loannner</C>.
                </Text>
              </View>
              <View style={loginStyle.line} />
              <FormLogin />
              <View style={loginStyle.mdpContainer}>
                <Text style={loginStyle.mdpForget} onPress={onPress}>
                  <C>Mot de passe oublié ?</C>.
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default Login;

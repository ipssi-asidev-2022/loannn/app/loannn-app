import {api, handleError} from '@utils/axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

/**
 * getMyStoredInfos
 * Get Current User Infos stored with AsyncStorage
 * Get info from async storage
 */
const getMyStoredInfos = async () => {
  try {
    let jsonValue = await AsyncStorage.getItem('@UserConnectionInfos');
    return jsonValue;
  } catch (error) {
    return handleError(error);
  }
};

/**
 * register
 * Post User registration
 * {{baseUrl}}/auth/local/register
 * body:
 * {
    "username": "exemple",
    "email": "exemple@exemple.fr",
    "password": "exemple"
    }
*/
const register = async body => {
  try {
    const {data} = await api.post('/auth/local/register', body);
    const jsonValue = JSON.stringify(data);
    await AsyncStorage.setItem('@UserConnectionInfos', jsonValue);
    return data;
  } catch (error) {
    return handleError(error);
  }
};

/**
 * login
 * Post login user
 * body:
  {
    "identifier": "exemple@exemple.fr",
    "password": "exemple"
  }
*/
const login = async body => {
  try {
    const {data} = await api.post('/auth/local', body);
    const jsonValue = JSON.stringify(data);
    await AsyncStorage.setItem('@UserConnectionInfos', jsonValue);
    return data;
  } catch (error) {
    return handleError(error);
  }
};

/**
 * logout
 * Logout a User
 * logout is only done locally on the client side.
 * This only requires you to remove the jwt & username token from your browser localStorage
 */
const logout = async () => {
  try {
    await AsyncStorage.removeItem('@UserConnectionInfos');
  } catch (error) {
    return handleError(error);
  }
};

export {getMyStoredInfos, register, login, logout};

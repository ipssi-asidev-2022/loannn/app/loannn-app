import {api} from '@utils/axios';

/** Get All Article
 * no token needed => roles: public
 * */
const getAllArticles = async () => {
  try {
    const {data} = await api.get('/articles');
    return data;
  } catch (error) {
    console.warn('error:', error.toJSON());
    return null;
  }
};

/** Post Article need token => roles: Authenticated
 * to post use user token with : Authorization => "Bearer "+jwt
 * "jwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.RIkLiJGKxVKNqwmMtWiYO5HSWzT6RRV63DW0PlkCpc8"
 * TODO: test to post an article with authenticated user
 */
// const PostArticle = async token => {
//   try {
//     configureApiBearerTokenHeaders(token);
//     const {data} = await api.get('/articles');
//     return data;
//   } catch (error) {
//     console.warn('error:', error);
//     return null;
//   }
// };

export {getAllArticles};

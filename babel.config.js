module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module:react-native-dotenv',
      {
        moduleName: '@env',
        path: '.env',
        blacklist: null,
        whitelist: null,
        safe: false,
        allowUndefined: true,
      },
    ],
    [
      '@babel/plugin-proposal-pipeline-operator',
      {
        proposal: 'hack',
        topicToken: '%',
      },
    ],
    [
      'module-resolver',
      {
        root: ['.'],
        extensions: ['.jsx', '.js', '.json', '.svg', '.jpg'],
        alias: {
          '@assets': './src/assets',
          '@components': './src/components',
          '@atoms': './src/components/atoms',
          '@molecules': './src/components/molecules',
          '@organisms': './src/components/organisms',
          '@screens': './src/screens',
          '@services': './src/services',
          '@utils': './src/utils',
          // '@interfaces': './src/interfaces',
          // '@routes': './src/routes',
          // '@stores': './src/stores',
          // '@styles': './src/styles',
        },
      },
    ],
  ],
};

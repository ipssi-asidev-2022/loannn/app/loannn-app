module.exports = {
  env: {
    es6: true,
    node: true,
    jest: true,
  },
  root: true,
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    '@react-native-community',
  ],
  plugins: ['react', 'react-hooks', 'prettier', 'no-loops'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  rules: {
    // 0 - turn the rule off
    // 1 - turn the rule on as a warning (doesn't affect exit code)
    // 2 - turn the rule on as an error (exit code is 1 when triggered)
    'prettier/prettier': 0,
    'no-trailing-spaces': 0,
    'comma-dangle': 0,
    'react/jsx-uses-react': 2,
    'react/jsx-uses-vars': 2,
    'no-loops/no-loops': 2,
  },
};
